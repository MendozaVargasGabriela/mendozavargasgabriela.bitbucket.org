<!DOCTYPE html>
<html>
  <head>
    <title>Mashup Google API </title>
    <link href="estilo.css" rel="stylesheet">
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset='utf-8' />
   <style>
       #map {
        height: 60%;
      width: 1040px;
       }
    </style>  </head>
  <body>
    <h1><center>Mashup Youtube, Maps, Calendar</h1>

    <!--Add buttons to initiate auth sequence and sign out-->
    <button id="authorize-button" style="display: none;">Autorizar</button>
    <button id="signout-button"  style="display: none;">Salir</button>

    <pre id="content"></pre>
    <hr>
  <div id="add">
    <label id="a">Titulo del evento:</label>
    <input type="text" id="titulo" size="50">
    <br>
    <label id="b">Fecha (YYYY-MM-DD):</label>
    <input type="text" id="fecha" size="50">
    <br>
    <label id="c">Hora inicio:</label>
    <input type="text" id="hstart" size="50">
    <br>
    <label id="d">Hora terminacion:</label>
    <input type="text" id="hend" size="50">
    <br>
    <label id="g">URL del video youtube:</label>
    <input type="text" id="description" size="50">
    <br>
    <br>
    <button id="agrega">Crear Evento</button>
    </div>
    <br>
    
    <div id="buttons">
 
        <input type="text" id="query" name="query" placeholder="Type something..." autocomplete="on" class="form-control" size="50"/>
          <button id="search-button" disabled onclick="search()">Search</button>
          <input id="numero_videos" value='1' type="number" maxlength="10" min="1"/>

      <button id="sig1" style="display: none;">1</button>
      <button id="sig2" style="display: none;">2</button>
      <button id="sig3" style="display: none;">3</button>
      <button id="sig4" style="display: none;">4</button>
      <button id="sig5" style="display: none;">5</button>
    </div>
    <hr>
    <br>

    <div id="results"></div>
    <hr>
    <br>
    
    <div id="map"></div>
    <div id="tweets"></div>      
   <script>
     var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: new google.maps.LatLng(30, 11)
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChFI7cJAqtdI7oNiHo1FX_LTNzHvh0LkM&callback=initMap">
    </script>
    
    <script>
    var CLIENT_ID = '71150300730-2uo6gtqtcp9sr7dblmdp74mp82d4r0ej.apps.googleusercontent.com';
    //71150300730-2uo6gtqtcp9sr7dblmdp74mp82d4r0ej.apps.googleusercontent.com    heroku
    //71150300730-pgrnq6auv70m3pj40b7csojk1rahn972.apps.googleusercontent.com    local
    //71150300730-ucini0i953kpaiif5g3qch35v09pdv6l.apps.googleusercontent.com    bitbucket
      var API_KEY = 'AIzaSyChFI7cJAqtdI7oNiHo1FX_LTNzHvh0LkM';
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];
      var SCOPES = "https://www.googleapis.com/auth/calendar.readonly " + "https://www.googleapis.com/auth/youtube.readonly " + "https://www.googleapis.com/auth/calendar ";

      var authorizeButton = document.getElementById('authorize-button');
      var signoutButton = document.getElementById('signout-button');
      var verevento = document.getElementById('ver-evento');
      var addButton = document.getElementById('agrega');
      var sig1 = document.getElementById('sig1');
      var sig2 = document.getElementById('sig2');
      var sig3 = document.getElementById('sig3');
      var sig4 = document.getElementById('sig4');
      var sig5 = document.getElementById('sig5');
      var boton_twitter = document.getElementById('boton_twitter');
      //boton_twitter.setAttribute("target","_blank");

      var more = '';
      var map;
      var video;
      var videos;
      var numero_resultados = 0;
      var numero_de_llamadas;

      function handleClientLoad() {
        gapi.load('client:auth2', initClient);
      }

      function initClient() {
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(function () {
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
          authorizeButton.onclick = handleAuthClick;
          signoutButton.onclick = handleSignoutClick;
          addButton.onclick = createEvent;
          sig1.onclick = muestra10;
          sig2.onclick = muestra20;
          sig3.onclick = muestra30;
          sig4.onclick = muestra40;
          sig5.onclick = muestra50;
        });
      }

      function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          authorizeButton.style.display = 'none';
          signoutButton.style.display = 'block';
          listUpcomingEvents();
          APILoaded();
        } else {
          authorizeButton.style.display = 'block';
          signoutButton.style.display = 'none';
        }
      }

      function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
      }

      function handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      function procesa_json_tweet (json) {
        var json_completo = JSON.parse(json);
        var div_tweets = document.getElementById("tweets");
        if(json_completo.length >0){
          for (var i = 0; i < json_completo.length; i++) {
            var tweet = json_completo[i];
            
            //if(tweet.place !== "null"){
              
              try {
                var coordinates = tweet.place.bounding_box.coordinates;
                var div = document.createElement("div");
                var img = document.createElement("img");
                img.src = tweet.user.profile_image_url;
                var parrafo = document.createElement("p");
                parrafo.innerHTML = ""+ tweet.created_at + "<h4> Nombre de usuario: "+tweet.user.screen_name + "</h4>"+ "" + tweet.text + "<hr>";
                div.appendChild(img);
                div.appendChild(parrafo);
                div_tweets.appendChild(div);
                saca_coordenadas_tweets(coordinates, tweet.user.screen_name, tweet.text);

              }
              catch (error){
                //console.log(error);
              }

            //}
          }
        }
        console.log(json_completo);
      }

      function saca_coordenadas_tweets(arreglo_coordenadas, nombre, texto){
          var coordenadas = arreglo_coordenadas[0];
          var latlong = coordenadas[0];
          var uluru = {
                      lat: parseFloat(latlong[1]), 
                      lng: parseFloat(latlong[0])
                    };
          puntea_tweets(uluru, nombre, texto);   
      }

      function puntea_tweets(latlng, nombre, texto){
        var infowindow = new google.maps.InfoWindow;
        var iconBase = 'twitter_logo.png';
        var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: iconBase //+ 'parking_lot_maps.png'
                });
        google.maps.event.addListener(marker, 'mouseover', function () {
        infowindow.setContent('@' + nombre + '<h3> tweet </h3>' + texto);
        infowindow.open(map, marker);
        });

        google.maps.event.addListener(marker, 'mouseout', function () {
        infowindow.close();
        });
      }

      function listUpcomingEvents() {
        var descripcion = '';
        gapi.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 10,
          'orderBy': 'startTime'
        }).then(function(response) {
          var events = response.result.items;
          appendPre('Upcoming events:');
          
          if (events.length > 0) {
            console.log(response.result.items);
            for (i = 0; i < events.length; i++) {
              var event = events[i];
              var when = event.start.dateTime;
              if (!when) {
                when = event.start.date;
              }
              if(event.description !== undefined){
                //console.log(descripcion);
                descripcion = event.description;
              }
              appendPre(event.summary + '. ' + descripcion + ' (' + when + ')')
              //console.log(event.description);
              //console.log(descripcion);
            }
          } else {
            appendPre('No hay eventos a mostrar para esta cuenta');
          }
        });
      }

      function createEvent(event) {
        var titulo = document.getElementById("titulo").value;
        var fecha = document.getElementById("fecha").value;
        var hstart = document.getElementById("hstart").value;
        var hend = document.getElementById("hend").value;
        var descript = document.getElementById("description").value;
        var video = '<a href='+ descript+'>'+ descript+'</a>';

        var empieza = fecha+'T'+hstart+':00-07:00';
        var termina = fecha+'T'+hend+':00-07:00';

        var resource = {
          "summary": titulo,
          "start": {
            "dateTime": empieza
          },
          "end": {
            "dateTime": termina
          },
          "description": video
        };
        var request = gapi.client.calendar.events.insert({
          'calendarId': 'primary',
          'resource': resource
        });
        request.execute(function(resp) {
          console.log(resp);
          alert("Evento agregado al calendario");
        });
      }

      function actualizar(){
        var pre = document.getElementById('results');
        while (pre.firstChild) {
        pre.removeChild(pre.firstChild);
        }

        var divmapa = document.getElementById('map');
        while (divmapa.firstChild) {
        divmapa.removeChild(divmapa.firstChild);
        }

        var divtweets = document.getElementById('tweets');
        while (divtweets.firstChild) {
        divtweets.removeChild(divtweets.firstChild);
        }

      }

      function search() {
        var n = 0;
        numero_resultados = document.getElementById("numero_videos").value; 
        video = "";
        videos = "";
        /*    
        actualizar();
        initMap();
        ejecutar();*/
        muestra_botones();
        
        var busqueda = document.getElementById('query').value;
        var q = $('#query').val();
        var request = gapi.client.youtube.search.list({
          part: "snippet",
          type: "video",
          id: "video",
          q: busqueda,
          maxResults: numero_resultados,
          order: "date"
        });

        request.execute(function (response) {
          var results = response.result.items;
          videos = results;
          var cont = document.getElementById("results");
          muestra10();
      });
    }

      function muestra_botones() {

          sig1.style.display = "none";
          sig2.style.display = "none";
          sig3.style.display = "none";
          sig4.style.display = "none";
          sig5.style.display = "none";
        
        if(numero_resultados>10 && numero_resultados<=20){
          sig1.style.display = "inline-block";
          sig2.style.display = "inline-block";
        }
        if(numero_resultados>20 && numero_resultados<=30){
          sig1.style.display = "inline-block";
          sig2.style.display = "inline-block";
          sig3.style.display = "inline-block";
        }
        if(numero_resultados>30 && numero_resultados<=40){
          sig1.style.display = "inline-block";
          sig2.style.display = "inline-block";
          sig3.style.display = "inline-block";
          sig4.style.display = "inline-block";
        }
        if(numero_resultados>40 && numero_resultados<=50){
          sig1.style.display = "inline-block";
          sig2.style.display = "inline-block";
          sig3.style.display = "inline-block";
          sig4.style.display = "inline-block";
          sig5.style.display = "inline-block";
        }
      }

      function muestra10(){

        //manda el nombre del boton
        actualizar();
        initMap();
        ejecutar();    

        if(numero_resultados<=10){
          for (var i = 0; i < numero_resultados; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
        else {
          for (var i = 0; i < 10; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
      }

      function muestra20() {
        actualizar();
        initMap();
        ejecutar();    

        if(numero_resultados<20){
          var espacio = numero_resultados-10;
          for (var i = 10; i < numero_resultados; i++) {
            muestra_videos(videos[i], espacio);
          }
        }
        else {
          for (var i = 10; i < 20; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
      }

      function muestra30() {
        ejecutar();    
        actualizar();
        initMap();

        if(numero_resultados<30){
          var espacio = numero_resultados-20;
          for (var i = 20; i < numero_resultados; i++) {
            muestra_videos(videos[i], espacio);
          }
        }
        else {
          for (var i = 20; i < 30; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
      }

      function muestra40() {
        ejecutar();    
        actualizar();
        initMap();

        if(numero_resultados<40){
          var espacio = numero_resultados-30;
          for (var i = 30; i < numero_resultados; i++) {
            muestra_videos(videos[i], espacio);
          }
        }
        else {
          for (var i = 30; i < 40; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
      }

      function muestra50() {
        ejecutar();    
        actualizar();
        initMap();

        if(numero_resultados<50){
          var espacio = numero_resultados-40;
          for (var i = 40; i < numero_resultados; i++) {
            muestra_videos(videos[i], espacio);
          }
        }
        else {
          for (var i = 40; i < 50; i++) {
            muestra_videos(videos[i], numero_resultados);
          }
        }
      }

      function muestra_videos(video_i, numero_resultados){
        var espacio = numero_resultados;
        if(numero_resultados>10){
          espacio = 10;
        }
        var cont = document.getElementById("results");
        var iframe = document.createElement("iframe");
            iframe.width=1000/espacio; 
            iframe.height="400";
            iframe.setAttribute('src','//www.youtube.com/embed/'+video_i.id.videoId);
            cont.appendChild(iframe);
            obten_videos_con_latitud_longitud(video_i);
      }

      function obten_videos_con_latitud_longitud(video) {
        var longitud=0;
        var latitud=0; 
        var request = gapi.client.youtube.videos.list({
          part : 'recordingDetails',
          id : video.id.videoId
        });
        request.execute(function (response){
          //console.log("Ga");
          //if(response.items !== 'undefined'){
            var elementos = response.items;
          //console.log(response.result.items);
          try{
            if(elementos[0].hasOwnProperty('recordingDetails')){
              if(elementos[0].recordingDetails.hasOwnProperty('location')){
                if(elementos[0].recordingDetails.location.hasOwnProperty('latitude')){
                  if(elementos[0].recordingDetails.location.hasOwnProperty('longitude')){
                    longitud=elementos[0].recordingDetails.location.longitude;
                    latitud=elementos[0].recordingDetails.location.latitude;
                    console.log(longitud);
                    console.log(latitud);
                    var uluru = {
                      lat: parseFloat(latitud), 
                      lng: parseFloat(longitud)
                    };
                    marca(uluru, video.snippet.title, video.id.videoId, video.id.publishedAt);
                  }
                }
              }
            }
          }
            catch(error) {
              //console.log(":c");
            } 
          //}
        });
      }

      function ad(event) {
        //manda el nombre del boton
        var clickedButton = event.originalTarget;
        var url = "https://youtu.be/"+ clickedButton.textContent;
        console.log(clickedButton.textContent);
        var descript = document.getElementById("description");
        descript.value = clickedButton.textContent;
        obten_videos_con_latitud_longitud(descript.value);
      }

      function marca(latlng, titulo_video, id_video, fecha) {
        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;
        var infowindowVideo = new google.maps.InfoWindow;

        geocoder.geocode({
          'location': latlng
        }, function (results, status) {
          if (status === 'OK') {
              if (results[1]) {
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });

                google.maps.event.addListener(marker, 'mouseover', function () {
                    infowindow.setContent('<h3>Titulo del Video: </h3>' + titulo_video  + '<h4>Dirección: </h4>' + results[1].formatted_address + '<iframe src=\"//www.youtube.com/embed/' + id_video + '\" allowfullscreen width="150" height="80"></iframe>');
                    infowindow.open(map, marker);
                });

                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close();
                });

            } else {
                window.alert('No se han encontrado resultados');
            }
        } 
    });
      }

      function ejecutar(){
            console.log("Inicia");
            var count = document.getElementById("query").value;
            $.ajax({
                url: "tweets.php",
                type: "POST",
                data: {num:count},
                success : function(json) {
                    //para que procese el json
                    //var contenido = JSON.parse(json);
                    procesa_json_tweet(json);
                    //var contenido = JSON.parse(json);
                    //console.log(contenido);
                    console.log("G");
                },


            });

        }

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: new google.maps.LatLng(30, 11)
        });
      }

      function mas_resultados (event) {
        actualizar();
        search();
      }

      function tweet (event){
        var url = "https://twitter.com/intent/tweet?";
        window.open(url,"width=500,height=300");
      }

      function APILoaded() {
        $('#search-button').attr('disabled', false);
      }
    </script>

    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
   
  </body>
</html>

